# Bean Validation

## Validation Groups

Default : jakarta.validation.groups.Default

```mermaid
classDiagram
    class Default
    class Group1
    class Group2
    Default <|-- Group2
```

## Model

```java
public record Model(
        @Size(max = 5)                         @Uppercase                         String field01,
        @Size(max = 5, groups = Default.class) @Uppercase                         String field02,
        @Size(max = 5, groups = Default.class) @Uppercase(groups = Default.class) String field03,
        @Size(max = 5, groups = Group1.class)  @Uppercase                         String field04,
        @Size(max = 5, groups = Group1.class)  @Uppercase(groups = Default.class) String field05,
        @Size(max = 5, groups = Group1.class)  @Uppercase(groups = Group1.class)  String field06,
        @Size(max = 5, groups = Group2.class)  @Uppercase                         String field07,
        @Size(max = 5, groups = Group2.class)  @Uppercase(groups = Default.class) String field08,
        @Size(max = 5, groups = Group2.class)  @Uppercase(groups = Group1.class)  String field09,
        @Size(max = 5, groups = Group2.class)  @Uppercase(groups = Group2.class)  String field10
) {
}

Model model = new Model("abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef");

```

## Validations

* ✔️ : is considered as valid
* ❌ : is considered as invalid

```java
getInValidFields(validate(model)); // 1
```

| Validation 1 | field01 | field02 | field03 | field04 | field05 | field06 | field07 | field08 | field09 | field10 |
|--------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Size         | ❌       | ❌       | ❌       | ✔️      | ✔️      | ✔️      | ✔️      | ✔️      | ✔️      | ✔️      |
| Uppercase    | ❌       | ❌       | ❌       | ❌      | ❌      | ✔️      | ❌      | ❌      | ✔️      | ✔️      |;

---

```java
getInValidFields(validate(model, Default.class)); // 2
```

| Validation 2 | field01 | field02 | field03 | field04 | field05 | field06 | field07 | field08 | field09 | field10 |
|--------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Size         | ❌       | ❌       | ❌       | ✔️      | ✔️      | ✔️      | ✔️      | ✔️      | ✔️      | ✔️      |
| Uppercase    | ❌       | ❌       | ❌       | ❌      | ❌      | ✔️      | ❌      | ❌      | ✔️      | ✔️      |

---

```java
getInValidFields(validate(model, Group1.class));  // 3
```

| Validation 3 | field01 | field02 | field03 | field04 | field05 | field06 | field07 | field08 | field09 | field10 |
|--------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Size         | ✔️       | ✔️       | ✔️       | ❌      | ❌      | ❌      | ✔️      | ✔️      | ✔️      | ✔️      |
| Uppercase    | ✔️       | ✔️       | ✔️       | ✔️      | ✔️      | ❌      | ✔️      | ✔️      | ❌      | ✔️      |

---

```java
getInValidFields(validate(model, Group2.class)); // 4
```

| Validation 4 | field01 | field02 | field03 | field04 | field05 | field06 | field07 | field08 | field09 | field10 |
|--------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Size         | ❌       | ❌       | ❌       | ✔️      | ✔️     | ✔️      | ❌      | ❌       | ❌      | ❌      |
| Uppercase    | ❌       | ❌       | ❌       | ❌      | ❌     | ✔️      | ❌      | ❌       | ✔️      | ❌      |

---

```java
getInValidFields(validate(model, Default.class, Group1.class)); // 5
```

| Validation 5 | field01 | field02 | field03 | field04 | field05 | field06 | field07 | field08 | field09 | field10 |
|--------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Size         | ❌       | ❌       | ❌       | ❌      | ❌     | ❌      | ✔️      | ✔️       | ✔️      | ✔️      |
| Uppercase    | ❌       | ❌       | ❌       | ❌      | ❌     | ❌      | ❌      | ❌       | ❌      | ✔️      |

---

```java
getInValidFields(validate(model, Default.class, Group2.class)); // 6
```

| Validation 6 | field01 | field02 | field03 | field04 | field05 | field06 | field07 | field08 | field09 | field10 |
|--------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Size         | ❌       | ❌       | ❌       | ✔️      | ✔️     | ✔️      | ❌      | ❌       | ❌      | ❌      |
| Uppercase    | ❌       | ❌       | ❌       | ❌      | ❌     | ✔️      | ❌      | ❌       | ❌      | ❌      |

---

```java
getInValidFields(validate(model, Group1.class, Group2.class)); // 7
```

| Validation 7 | field01 | field02 | field03 | field04 | field05 | field06 | field07 | field08 | field09 | field10 |
|--------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Size         | ❌       | ❌       | ❌       | ❌      | ❌     | ❌      | ❌      | ❌       | ❌      | ❌      |
| Uppercase    | ❌       | ❌       | ❌       | ❌      | ❌     | ❌      | ❌      | ❌       | ❌      | ❌      |

---

```java
getInValidFields(validate(model, Default.class, Group1.class, Group2.class)); // 8
```

| Validation 8 | field01 | field02 | field03 | field04 | field05 | field06 | field07 | field08 | field09 | field10 |
|--------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Size         | ❌       | ❌       | ❌       | ❌      | ❌     | ❌      | ❌      | ❌       | ❌      | ❌      |
| Uppercase    | ❌       | ❌       | ❌       | ❌      | ❌     | ❌      | ❌      | ❌       | ❌      | ❌      |
