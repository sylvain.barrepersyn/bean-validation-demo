package com.sbp;

import com.sbp.Groups.Group1;
import com.sbp.Groups.Group2;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.groups.Default;

import java.util.ArrayList;
import java.util.List;

import static jakarta.validation.Validation.buildDefaultValidatorFactory;
import static java.util.stream.Collectors.toList;

public class App {

    public static void main(String[] args) {

        Model model = new Model("abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef", "abcdef");

        System.out.println(getInValidFields(validate(model)));

        System.out.println(getInValidFields(validate(model, Default.class)));

        System.out.println(getInValidFields(validate(model, Group1.class)));

        System.out.println(getInValidFields(validate(model, Group2.class)));

        System.out.println(getInValidFields(validate(model, Default.class, Group1.class)));

        System.out.println(getInValidFields(validate(model, Default.class, Group2.class)));

        System.out.println(getInValidFields(validate(model, Group1.class, Group2.class)));

        System.out.println(getInValidFields(validate(model, Default.class, Group1.class, Group2.class)));
    }

    public static List<ConstraintViolation<Model>> validate(Model model, Class<?>... groups) {
        return new ArrayList<>(buildDefaultValidatorFactory().getValidator()
                                                             .validate(model, groups));
    }

    public static List<?> getInValidFields(List<ConstraintViolation<Model>> violations) {
        return violations
                .stream()
                .map(v -> v.getPropertyPath()
                           .toString() + " - " + v.getMessage())
                .sorted()
                .collect(toList());
    }
}
