package com.sbp;

import jakarta.validation.groups.Default;

public final class Groups {

    public interface Group1 {
    }

    public interface Group2 extends Default {
    }

    private Groups() {
    }
}
