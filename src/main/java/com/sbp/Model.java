package com.sbp;

import com.sbp.Groups.Group1;
import com.sbp.Groups.Group2;
import com.sbp.constraints.Uppercase;
import jakarta.validation.constraints.Size;
import jakarta.validation.groups.Default;

public record Model(
        @Size(max = 5) @Uppercase String field01,
        @Size(max = 5, groups = Default.class) @Uppercase String field02,
        @Size(max = 5, groups = Default.class) @Uppercase(groups = Default.class) String field03,
        @Size(max = 5, groups = Group1.class) @Uppercase String field04,
        @Size(max = 5, groups = Group1.class) @Uppercase(groups = Default.class) String field05,
        @Size(max = 5, groups = Group1.class) @Uppercase(groups = Group1.class) String field06,
        @Size(max = 5, groups = Group2.class) @Uppercase String field07,
        @Size(max = 5, groups = Group2.class) @Uppercase(groups = Default.class) String field08,
        @Size(max = 5, groups = Group2.class) @Uppercase(groups = Group1.class) String field09,
        @Size(max = 5, groups = Group2.class) @Uppercase(groups = Group2.class) String field10
) {
}
