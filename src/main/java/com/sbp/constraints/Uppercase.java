package com.sbp.constraints;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = UppercaseValidator.class)
@Target({PARAMETER, FIELD})
@Retention(RUNTIME)
public @interface Uppercase {

    String message() default "Field must be uppercase.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
